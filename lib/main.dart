import 'package:demofoodui/screen/details_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo food',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Project food'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _basketAmount = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFebac3f),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 20.0,
                  ),
                  Container(
                    width: 125.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.layers_clear),
                          color: Colors.white,
                          onPressed: () {
                            setState(() {
                              _basketAmount = 0;
                            });
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.menu),
                          color: Colors.white,
                          onPressed: () {
                            //to do back
                          },
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 25.0),
            Padding(
              padding: EdgeInsets.only(left: 40.0),
              child: Row(
                children: [
                  Text(
                    'Healthy',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(width: 10.0),
                  Text(
                    'Food',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Colors.white,
                        fontSize: 25.0),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(75.0))),
                      child: Padding(
                        padding:
                            EdgeInsets.only(left: 30.0, top: 45.0, right: 30.0),
                        child: Container(
                          child: ListView(
                            children: [
                              _buildFoodItem(
                                  "assets/avocado.png", "Avocado", "2.00"),
                              _buildFoodItem(
                                  "assets/banana.png", "Banana", "1.50"),
                              _buildFoodItem(
                                  "assets/dragon.png", "Dragon", "4.00"),
                              _buildFoodItem(
                                  "assets/fruit.png", "Apple", "1.70"),
                              _buildFoodItem("assets/kiwi.png", "Kiwi", "2.30"),
                              _buildFoodItem(
                                  "assets/orange.png", "Orange", "1.40"),
                              _buildFoodItem("assets/pomegranate.png",
                                  "Pomegranate", "2.80"),
                              _buildFoodItem("assets/strawberry.png",
                                  "Strawberry", "4.00"),
                              _buildFoodItem("assets/olive_oil.png",
                                  "Fresh sushi", "4.40"),
                              _buildFoodItem(
                                  "assets/meat.png", "Stake", "7.30"),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  _buildBottomActionWidget()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomActionWidget() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(
            left: 40.0, top: 5.0, right: 40.0, bottom: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 60.0,
              width: 60.0,
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 1.0,
                      style: BorderStyle.solid,
                      color: Colors.grey),
                  borderRadius: BorderRadius.circular(30.0)),
              child: Center(
                child: Icon(
                  Icons.search,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              height: 60.0,
              width: 120.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  border: Border.all(
                      color: Colors.grey,
                      width: 1.0,
                      style: BorderStyle.solid),
                  color: Color(0xFF1C1428)),
              child: Center(
                child: Text(
                  'Checkout',
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.white,
                      fontSize: 15.0),
                ),
              ),
            ),
            Container(
              height: 60.0,
              width: 60.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  border: Border.all(
                      color: Colors.grey,
                      width: 1.0,
                      style: BorderStyle.solid)),
              child: Stack(children: [
                Center(
                  child: Icon(
                    Icons.shopping_basket,
                    color: Colors.black,
                  ),
                ),
                Positioned(
                  top: 10.0,
                  left: 40.0,
                  child: Text(
                    _basketAmount.toString(),
                    style: TextStyle(
                        fontSize: 12.0, fontFamily: 'Montserrat'),
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildFoodItem(String imagePath, String name, String price) {
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  DetailsScreen(tag: imagePath, name: name, price: price)));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Row(
                children: [
                  Hero(
                    tag: imagePath,
                    child: Image(
                      fit: BoxFit.cover,
                      image: AssetImage(imagePath),
                      height: 75.0,
                      width: 75.0,
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16.5,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "\$$price",
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16.5,
                            color: Colors.grey),
                      )
                    ],
                  )
                ],
              ),
            ),
            IconButton(
              icon: Icon(Icons.add),
              color: Colors.black,
              onPressed: () {
                setState(() {
                  _basketAmount += 1;
                });
              },
            )
          ],
        ),
      ),
    );
  }
}
