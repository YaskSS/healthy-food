import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DetailsScreen extends StatefulWidget {
  final tag;
  final name;
  final String price;

  DetailsScreen({Key key, this.tag, this.name, this.price}) : super(key: key);

  @override
  State<StatefulWidget> createState() => DetailsScreenState();
}

class DetailsScreenState extends State<DetailsScreen> {
  String selectedCard = "WEIGHT";
  int quantity = 1;
  String amount;

  @override
  void initState() {
    amount = widget.price;
    super.initState();
  }

  Widget _buildInfoFoodListWidget() {
    return Container(
      color: Colors.white,
      child: Container(
        height: 100.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            _buildInfoCard("WEIGHT", '300', 'G'),
            _buildInfoCard("CALORIES", '267', 'CAL'),
            _buildInfoCard("VITAMINS", 'A, B2', 'VIT'),
            _buildInfoCard("AVAIL", 'NO', 'AV'),
            _buildInfoCard("PROTEIN", '10', 'G'),
            _buildInfoCard("FAT", '0.100', 'G'),
            _buildInfoCard("CARBOHIDRATOS", '0.50', 'G'),
          ],
        ),
      ),
    );
  }

  Widget _buildBuyBtnWidget() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(45.0),
            bottomRight: Radius.circular(54.0),
          )),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding:
            EdgeInsets.only(top: 18.0, bottom: 22.0, left: 18.0, right: 18.0),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xFF1C1428),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(35.0),
                    topRight: Radius.circular(35.0),
                    bottomLeft: Radius.circular(35.0),
                    bottomRight: Radius.circular(35.0))),
            child: Padding(
              padding: EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "\$ $amount",
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildChooseCountWidget() {
    return Container(
      width: 125.0,
      height: 40.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16.5), color: Color(0xFFebac3f)),
      child: Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  if (quantity > 0) {
                    --quantity;
                    amount = (double.parse(amount) - double.parse(widget.price))
                        .toStringAsFixed(2); //todo move to method
                  }
                });
              },
              child: Container(
                width: 25.0,
                height: 25.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7.0),
                    color: Color(0xFFebac3f),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26,
                          blurRadius: 6.0,
                          offset: Offset(0, 2))
                    ]),
                child: Center(
                  child: Icon(
                    Icons.remove,
                    color: Colors.white,
                    size: 20.0,
                  ),
                ),
              ),
            ),
            Text(
              quantity.toString(),
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Colors.white,
                  fontSize: 15.0),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  ++quantity;
                  amount = (double.parse(amount) + double.parse(widget.price))
                      .toStringAsFixed(2);
                });
              },
              child: Container(
                height: 25.0,
                width: 25.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(7.0)),
                child: Center(
                  child: Icon(
                    Icons.add,
                    color: Color(0xFFebac3f),
                    size: 20.0,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFebac3f),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          'Details',
          style: TextStyle(
              fontFamily: 'Montserrat', color: Colors.white, fontSize: 18.0),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: [
          IconButton(
            icon: Icon(Icons.more_horiz),
            onPressed: () {},
            color: Colors.white,
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height - 80,
                    width: MediaQuery.of(context).size.width,
                  ),
                  Positioned(
                    top: 75.0,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(45.0),
                              topRight: Radius.circular(45.0)),
                          color: Colors.white),
                      height: MediaQuery.of(context).size.height - 100,
                      width: MediaQuery.of(context).size.width,
                    ),
                  ),
                  Positioned(
                      top: 30.0,
                      left: MediaQuery.of(context).size.width / 2 - 100,
                      child: Container(
                        child: Hero(
                          tag: widget.tag,
                          child: Image(image: AssetImage(widget.tag)),
                        ),
//                        decoration: BoxDecoration(
//                            image: DecorationImage(
//                                image: AssetImage(widget.tag),
//                                fit: BoxFit.cover)),
                        height: 200.0,
                        width: 200.0,
                      )),
                  Positioned(
                    top: 250.0,
                    left: 25.0,
                    right: 25.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.name,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "\$ ${widget.price}",
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 20.0,
                                  color: Colors.grey),
                            ),
                            Container(
                              height: 25.0,
                              width: 1.0,
                              color: Colors.grey,
                            ),
                            _buildChooseCountWidget()
                          ],
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(top: 15.0),
                            child: Text(
                              'Some information about current item. How we can to eat this item. Why we shoud buy this and what of kind. We really recomend to buy this product',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'Montserrat',
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            _buildInfoFoodListWidget(),
            _buildBuyBtnWidget()
          ],
        ),
      ),
    );
  }

  Widget _buildInfoCard(String title, String info, String unit) {
    return Padding(
      padding: EdgeInsets.only(left: 5.0, right: 5.0),
      child: InkWell(
        onTap: () {
          _selectCard(title);
        },
        child: AnimatedContainer(
          duration: Duration(milliseconds: 250),
          curve: Curves.easeIn,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: title == selectedCard ? Color(0xFFebac3f) : Colors.white,
              border: Border.all(
                  color: title == selectedCard
                      ? Colors.transparent
                      : Colors.grey.withOpacity(0.3),
                  style: BorderStyle.solid,
                  width: 0.75)),
          height: 100.0,
          width: 100.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 15.0),
                child: Text(title,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 12.0,
                      color: title == selectedCard
                          ? Colors.white
                          : Colors.grey.withOpacity(0.7),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, bottom: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(info,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 14.0,
                            color: title == selectedCard
                                ? Colors.white
                                : Colors.black,
                            fontWeight: FontWeight.bold)),
                    Text(unit,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12.0,
                          color: title == selectedCard
                              ? Colors.white
                              : Colors.black,
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _selectCard(String cardTitle) {
    setState(() {
      selectedCard = cardTitle;
    });
  }
}
